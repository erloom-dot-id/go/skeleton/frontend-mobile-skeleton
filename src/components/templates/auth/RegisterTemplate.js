import {Colors, Typography} from '../../../utils';
import {Content, Gap, InputText, TextBlack, TextBold, TextRegular, TextSemiBold, Touchable} from '../..';
import {View} from 'react-native';
import {GoogleIcon, FacebookIcon, HomeIcon} from '../../../assets';

const RegisterTemplate = ({navigation}) => {
  return (
    <View className="p-5">
      <View className="items-center">
        <Gap height={38}/>
        <TextSemiBold text="Daftar" size={24} />
        <View className="flex-row mt-4">
          <TextRegular text="Sudah memiliki akun ?" />
          <Touchable onPress={() => navigation.navigate('Login')}>
            <TextRegular text=" Masuk" color={Colors.BLUE700} />
          </Touchable>
        </View>
      </View>
      <Gap height={32} />
      <View className="flex-row">
        <TextSemiBold text="Nama Lengkap" size={14} />
        <TextRegular text=" *" color={Colors.RED700} size={18} />
      </View>
      <InputText placeholder="Nama Lengkap" borderColorFocus={Colors.BLUE700} />
      <Gap height={16} />
      <View className="flex-row">
        <TextSemiBold text="Email" size={14} />
        <TextRegular text=" *" color={Colors.RED700} size={18} />
      </View>
      <InputText placeholder="Email" borderColorFocus={Colors.BLUE700} />
      <Gap height={16} />
      <View className="flex-row">
        <TextSemiBold text="Kata Sandi" size={14} />
        <TextRegular text=" *" color={Colors.RED700} size={18} />
      </View>
      <InputText placeholder="Kata Sandi" inputType="password" borderColorFocus={Colors.BLUE700} />
      <TextRegular text="* Minimal 8 Karakter" className="mt-2 ml-1" />
      <TextRegular text="* Tidak boleh menggunakan karakter spesial" className="ml-1" />
      <Gap height={16} />
      <View className="flex-row">
        <TextSemiBold text="Konfirmasi Kata Sandi" size={14} />
        <TextRegular text=" *" color={Colors.RED700} size={18} />
      </View>
      <InputText placeholder="Konfirmasi Kata Sandi" inputType="password" borderColorFocus={Colors.BLU700} />
      <Gap height={32} />
      <View className="flex-row">
        <Touchable className="h-5 w-5 border rounded border-gray-300 mx-1"></Touchable>
        <TextRegular text=" Saya menyetujui" size={14} />
        <TextBold text=" Syarat & Ketentuan" size={14} />
      </View>
      <Gap height={32} />

      <Touchable
        onPress={() => console.log("daftar")}
        className="w-full bg-blue-700 h-10 rounded-md justify-center items-center">
        <TextRegular text="Daftar" color="white" />
      </Touchable>
    </View>
  );
};

export default RegisterTemplate;
