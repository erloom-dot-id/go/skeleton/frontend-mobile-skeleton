import {Colors, Typography} from '../../../utils';
import {Content, Gap, InputText, TextBlack, TextBold, TextRegular, TextSemiBold, Touchable} from '../../../components';
import {Text, View} from 'react-native';

const LoginTemplate = ({navigation}) => {
  return (
    <View className="p-5">
      <View className="items-center mt-12">
        <TextSemiBold text="Masuk Ke Akun" size={24} />
        <View className="flex-row mt-4">
          <TextRegular text="Belum memiliki akun?" />
          <Touchable onPress={() => navigation.navigate('Register')}>
            <TextRegular text=" Daftar" color={Colors.BLUE700} />
          </Touchable>
        </View>
      </View>
      <TextRegular text="Email" className="mt-12 font-bold" color={"#00000"}/>
      <InputText className="mt-2" placeholder="Email" borderColorFocus={Colors.BLUE700} keyboardType="email-address"/>
      <TextBlack text="Password" className="mt-5 font-bold" />
      <InputText placeholder="password" borderColorFocus={Colors.BLUE700} inputType="password" />
      <Touchable className="mt-5 self-end" onPress={() => console.log('AturSandi')}>
        <TextRegular text="Lupa Kata sandi?" color={Colors.BLUE700} />
      </Touchable>
      <Touchable
        onPress={() => console.log('BottomTab')}
        className="w-full bg-blue-700 h-10 rounded-md justify-center items-center mt-12">
        <TextRegular text="Masuk" color="white" />
      </Touchable>
      <View className=" flex-row mt-12">
        <View className=" flex-1 self-center bg-gray-300" style={{height: 1}}></View>
        <TextRegular text="atau masuk dengan" className="px-3" color={Colors.GREY700} size={Typography.FONT_SIZE_12} />
        <View className=" flex-1 self-center bg-gray-300" style={{height: 1}}></View>
      </View>
      <View className="flex-row mt-12 justify-between">
        <View className="flex-1 flex-row border h-10 border-gray-300 rounded-md items-center justify-center mr-4">
          <TextRegular text=" Google" />
        </View>
        <View className="flex-1 flex-row border h-10 border-gray-300 rounded-md items-center justify-center">
          <TextRegular text=" Facebook" />
        </View>
      </View>
    </View>
  );
};

export default LoginTemplate;
