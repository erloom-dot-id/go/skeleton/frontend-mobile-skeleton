import React from 'react';
import {Colors, Typography} from '../../utils';
import {TextSemiBold} from '../atom/Text';
import {Touchable} from '../atom/Touchable';
import PropTypes from 'prop-types';
import {View} from 'react-native';
import {Gap} from '../../components';

// -------------------- BUTTON FILLED-------------------- //
export const ButtonFilled = props => {
  const {
    text,
    className,
    style,
    textColor,
    textSize,
    onPress,
    backgroundColor,
    icon,
    textStyle,
    disabledBackgroundColor,
    disabled,
  } = props;
  return (
    <Touchable
      disabled={disabled}
      onPress={onPress}
      className={`rounded-xl p-3 justify-center flex-row ${className}`}
      style={[style, {backgroundColor: disabled ? disabledBackgroundColor : backgroundColor}]}>
      {icon && <View className="mr-2">{icon}</View>}
      <TextSemiBold text={text} color={textColor} size={textSize} style={textStyle}>
        {text}
      </TextSemiBold>
    </Touchable>
  );
};

ButtonFilled.defaultProps = {
  borderColor: Colors.RED900,
  backgroundColor: Colors.RED900,
  textSize: Typography.FONT_SIZE_14,
  text: 'Button Filled',
  className: 'py-4',
  textColor: Colors.NETRAL50,
};

ButtonFilled.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.any,
  textSize: PropTypes.any,
  onPress: PropTypes.func,
  textColor: PropTypes.any,
  backgroundColor: PropTypes.any,
  icon: PropTypes.any,
  textStyle: PropTypes.any,
  disabledBackgroundColor: PropTypes.any,
  disabled: PropTypes.any,
};

// -------------------- BUTTON OUTLINE-------------------- //
export const ButtonOutline = props => {
  const {text, className, style, borderColor, textSize, onPress, backgroundColor, textColor, leftIcon} = props;
  return (
    <Touchable
      onPress={onPress}
      className={`rounded-xl border flex-row items-center justify-center p-3 ${className}`}
      style={[style, {backgroundColor: backgroundColor, borderColor: borderColor}]}>
      {leftIcon && (
        <View>
          <View>{leftIcon}</View>
          <Gap width={10} />
        </View>
      )}
      <TextSemiBold text={text} color={textColor} size={textSize} />
    </Touchable>
  );
};

ButtonOutline.defaultProps = {
  borderColor: Colors.RED900,
  textSize: Typography.FONT_SIZE_12,
  text: 'Button Outline',
  backgroundColor: Colors.TRANSPARENT,
  textColor: Colors.RED900,
};

ButtonOutline.propTypes = {
  text: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.any,
  borderColor: PropTypes.any,
  textSize: PropTypes.any,
  onPress: PropTypes.func,
  backgroundColor: PropTypes.any,
  textColor: PropTypes.any,
  leftIcon: PropTypes.any,
};
