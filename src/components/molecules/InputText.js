import {TextRegular, TextSemiBold} from '../atom/Text';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, TextInput, TouchableOpacity, View} from 'react-native';
import {Colors, Mixins, Typography} from '../../utils';
import PropTypes from 'prop-types';
import {EyeCloseIcon, EyeOpenIcon, CopyIcon} from '../../assets';
import {Gap, Touchable} from '../../components';

export const InputText = props => {
  const {
    className,
    inputRef,
    borderColorFocus,
    textAlignVertical,
    onKeyPress,
    textInputStyle,
    borderRadius,
    borderColor,
    backgroundColor,
    placeholderErr,
    placeholder,
    onIconClick = () => console.log('Icon Click'),
    onBlur = () => console.log('onBlur'),
    inputValue,
    colorInputValue,
    errText,
    minHeight,
    isErr,
    isSuccess,
    isValid,
    isMultiline,
    isEditable,
    maxLength,
    keyboardType,
    inputType,
    disableTextColor,
    style,
    placeholderTextColor,
    label,
    onChange,
    leftIcon,
    rightIcon,
    enterKeyHint,
    onSubmitEditing = () => console.log('submit editing'),
    required,
    onPressLeftIcon = () => console.log('Left IconClick'),
    innerLabel,
    labelSize,
    inputMode,
    onPress,
  } = props;
  const [focus, setFocus] = useState(false);
  const [edit, setEdit] = useState(true);
  const [isSecure, setIsSecure] = useState(false);
  useEffect(() => {
    if (inputType === 'password') {
      setIsSecure(true);
    }
  }, []);

  useEffect(() => {
    if (isEditable !== undefined) {
      setEdit(isEditable);
    }
  }, [isEditable]);

  return (
    <Touchable style={[style]} onPress={onPress} disabled={!onPress}>
      {innerLabel ? (
        <View className="absolute z-10 -top-2.5 left-2 px-1 bg-white transition-transform rounded-md">
          {(focus && label) || (inputValue && label) ? (
            <View>
              <Text>
                <TextRegular text={label} size={Typography.FONT_SIZE_12} color={isErr ? Colors.R400 : Colors.G400} />
                <Gap width={3} />
                {required && <TextRegular text="*" color={Colors.R400} />}
              </Text>
            </View>
          ) : null}
        </View>
      ) : (
        <View>
          {label ? (
            <Text>
              <TextRegular text={label} size={labelSize} color={Colors.G400} />
              {required && <TextRegular text=" *" color={Colors.R400} size={labelSize} />}
            </Text>
          ) : null}
        </View>
      )}

      <View
        style={[
          isMultiline ? styles.inputMutiline : styles.textInput,
          {
            borderColor: isErr ? Colors.RED900 : focus ? borderColorFocus : isSuccess ? Colors.GREEN500 : borderColor,
            width: '100%',
            borderRadius: borderRadius,
            borderWidth: 1,
            backgroundColor: backgroundColor,
          },
        ]}>
        {leftIcon && (
          <Touchable onPress={onPressLeftIcon} style={{marginRight: 8}}>
            {leftIcon}
          </Touchable>
        )}
        {inputType === 'phone' && (
          <View style={styles.phoneNumberWrapper}>
            <TextSemiBold
              text={'+62'}
              color={Colors.NETRAL900}
              size={Typography.FONT_SIZE_14}
              style={{marginRight: 10}}
            />
          </View>
        )}
        <TextInput
          className={className}
          accessible={true}
          ref={inputRef}
          onFocus={() => (!isEditable ? setFocus(false) : setFocus(true))}
          onBlur={() => {
            onBlur();
            setFocus(false);
          }}
          onSubmitEditing={onSubmitEditing}
          enterKeyHint={enterKeyHint}
          editable={edit}
          inputMode={inputMode}
          value={inputValue}
          multiline={isMultiline}
          placeholder={label && innerLabel ? (focus ? placeholder : label) : placeholder}
          maxLength={maxLength}
          placeholderTextColor={isErr && placeholderErr ? Colors.R400 : placeholderTextColor}
          keyboardType={keyboardType}
          secureTextEntry={isSecure}
          style={[
            {
              textAlignVertical: textAlignVertical,
              minHeight: minHeight,
              flex: 1,
              width: '100%',
              color: isEditable ? colorInputValue : disableTextColor ? disableTextColor : Colors.N10P,
            },
            textInputStyle,
          ]}
          onChangeText={onChange}
          onKeyPress={onKeyPress}
        />
        {/* {inputType === 'password' && (
          <TouchableOpacity accessible={true} activeOpacity={0.5} onPress={() => setIsSecure(!isSecure)}>
            {isSecure ? (
              <EyeCloseIcon height={25} width={25} color={Colors.NETRAL100} />
            ) : (
              <EyeOpenIcon height={25} width={25} color={Colors.NETRAL100} />
            )}
          </TouchableOpacity>
        )} */}
        {inputType === 'copy' && (
          <TouchableOpacity accessible={true} activeOpacity={0.5} onPress={onIconClick}>
            <CopyIcon />
          </TouchableOpacity>
        )}
        {inputType === 'menu' && (
          <TouchableOpacity style={{marginRight: -10}} accessible={true} activeOpacity={0.5} onPress={onIconClick}>
            {rightIcon}
          </TouchableOpacity>
        )}
      </View>
      {isErr || isValid ? (
        <View style={{marginTop: 2, marginBottom: -5}}>
          <TextRegular text={errText} size={Typography.FONT_SIZE_10} color={isValid ? Colors.G400 : Colors.R400} />
        </View>
      ) : (
        <View />
      )}
    </Touchable>
  );
};

InputText.defaultProps = {
  colorInputValue: Colors.NETRAL900,
  minHeight: 40,
  isErr: false,
  isSuccess: false,
  isValid: false,
  isMultiline: false,
  isEditable: true,
  keyboardType: 'default',
  disableTextColor: Colors.NETRAL100,
  placeholderTextColor: Colors.NETRAL100,
  borderRadius: 8,
  iconColor: Colors.NETRAL100,
  backgroundColor: Colors.NETRAL0,
  textAlignVertical: 'center',
  borderColorFocus: Colors.RED900,
  required: false,
  innerLabel: false,
  labelSize: Typography.FONT_SIZE_14,
  borderColor: Colors.NETRAL100,
};

InputText.propTypes = {
  className: PropTypes.string,
  placeholder: PropTypes.string,
  onIconClick: PropTypes.any,
  onBlur: PropTypes.any,
  inputValue: PropTypes.any,
  colorInputValue: PropTypes.any,
  errText: PropTypes.string,
  minHeight: PropTypes.any,
  isErr: PropTypes.bool,
  isSuccess: PropTypes.bool,
  isValid: PropTypes.bool,
  isMultiline: PropTypes.bool,
  isEditable: PropTypes.bool,
  maxLength: PropTypes.any,
  keyboardType: PropTypes.any,
  inputType: PropTypes.string,
  disableTextColor: PropTypes.any,
  style: PropTypes.any,
  placeholderTextColor: PropTypes.any,
  borderRadius: PropTypes.any,
  borderColor: PropTypes.any,
  backgroundColor: PropTypes.any,
  placeholderErr: PropTypes.any,
  inputRef: PropTypes.any,
  borderColorFocus: PropTypes.any,
  textAlignVertical: PropTypes.any,
  onKeyPress: PropTypes.any,
  textInputStyle: PropTypes.any,
  label: PropTypes.string,
  onChange: PropTypes.any,
  leftIcon: PropTypes.any,
  rightIcon: PropTypes.any,
  enterKeyHint: PropTypes.string,
  onSubmitEditing: PropTypes.func,
  required: PropTypes.bool,
  onPressLeftIcon: PropTypes.any,
  innerLabel: PropTypes.bool,
  labelSize: PropTypes.any,
  inputMode: PropTypes.string,
  onPress: PropTypes.any,
};

export const InputButton = props => {
  const {
    text,
    style,
    onPress,
    placeholderText,
    icon,
    textColor,
    placeholderSize,
    rightIcon,
    borderRadius,
    showPlaceholder,
    disabled,
    darkBackground,
  } = props;
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.inputButtonStyle, style, {borderRadius: borderRadius}]}
      disabled={disabled}
      activeOpacity={0.7}>
      <View style={[styles.textInput]}>
        {showPlaceholder && (
          <View style={darkBackground ? styles.labelDarkBackground : styles.label}>
            <TextSemiBold text={placeholderText} size={placeholderSize} color={Colors.NETRAL100} />
          </View>
        )}
        {icon ? icon : null}
        {text !== '' ? (
          <TextRegular text={text} color={textColor} size={Typography.FONT_SIZE_14} numberOfLines={1} />
        ) : (
          <TextRegular text={placeholderText} color={Colors.NETRAL100} size={Typography.FONT_SIZE_14} />
        )}
      </View>
      {rightIcon && (
        <View
          style={{
            width: '10%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          {rightIcon}
        </View>
      )}
    </TouchableOpacity>
  );
};

InputButton.defaultProps = {
  text: '',
  placeholderText: 'placeholder',
  textColor: Colors.NETRAL100,
  placeholderSize: Typography.FONT_SIZE_12,
  borderRadius: 8,
  showPlaceholder: false,
  disabled: false,
  darkBackground: false,
};

InputButton.propTypes = {
  text: PropTypes.string,
  style: PropTypes.any,
  onPress: PropTypes.any,
  placeholderText: PropTypes.string,
  icon: PropTypes.any,
  textColor: PropTypes.any,
  placeholderSize: PropTypes.any,
  rightIcon: PropTypes.any,
  borderRadius: PropTypes.any,
  showPlaceholder: PropTypes.any,
  disabled: PropTypes.any,
  darkBackground: PropTypes.any,
};

const styles = StyleSheet.create({
  ddContainer: {
    marginBottom: 16,
    flex: 1,
  },
  labelContainer: {
    marginBottom: 4,
  },
  dropdownLabel: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#25324B',
  },
  container: {
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  content: {
    // height: 40,
    fontSize: Mixins.scaleFont(14),
    paddingBottom: 0,
    paddingLeft: 0,
  },
  rightButton: {
    // width: '15%',
    width: 35,
    alignItems: 'center',
    alignSelf: 'flex-end',
    paddingVertical: Mixins.scaleSize(5),
  },
  floating: {
    position: 'absolute',
    top: 10,
  },
  message: {
    position: 'absolute',
    bottom: -17,
    right: 0,
  },
  label: {
    position: 'absolute',
    top: -8,
    zIndex: 3,
    left: 10,
    backgroundColor: Colors.NETRAL0,
    // paddingHorizontal: 4,
  },
  labelDarkBackground: {
    position: 'absolute',
    top: -8,
    zIndex: 3,
    left: 10,
    backgroundColor: Colors.NETRAL900,
    paddingHorizontal: 4,
  },
  textInput: {
    width: '90%',
    paddingHorizontal: 10,
    paddingRight: 20,
    height: 45,
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputDefault: {
    borderColor: Colors.NETRAL50,
  },
  inputMutiline: {
    width: '90%',
    padding: 10,
    flexDirection: 'row',
  },
  phoneNumberWrapper: {
    borderRightWidth: 1,
    justifyContent: 'center',
    borderRightColor: Colors.NETRAL100,
    marginRight: 12,
  },
  inputButtonStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: Colors.NETRAL50,
    borderWidth: 1,
    borderRadius: 4,
  },
  required: {
    color: 'red',
  },
  dropdownContainer: {
    borderRadius: 8,
    borderWidth: 1,
    borderColor: Colors.NETRAL50,
    backgroundColor: Colors.NETRAL0,
    paddingHorizontal: 4,
  },
  dropdown: {
    backgroundColor: Colors.NETRAL0,
  },
  itemDropdown: {
    fontSize: Typography.FONT_SIZE_14,
    backgroundColor: Colors.NETRAL0,
  },
});
