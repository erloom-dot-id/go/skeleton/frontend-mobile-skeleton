import React from 'react';
import {Platform, StatusBar} from 'react-native';
import {useIsFocused} from '@react-navigation/native';
import PropTypes from 'prop-types';

const FocusAwareStatusBar = ({barStyle, backgroundColor}) => {
  const isFocused = useIsFocused();
  return isFocused ? (
    <StatusBar
      barStyle={Platform.OS === 'ios' ? 'dark-content' : barStyle}
      translucent
      backgroundColor={backgroundColor}
      showHideTransition
      animated
    />
  ) : null;
};

FocusAwareStatusBar.propTypes = {
  barStyle: PropTypes.any,
  backgroundColor: PropTypes.any,
};
export default FocusAwareStatusBar;
