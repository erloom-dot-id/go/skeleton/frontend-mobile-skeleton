import React from 'react';
import {TouchableNativeFeedback, View, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';

export const FilledTouchable = props => {
  const {children, onPress, disabled, style, containerStyle} = props;
  return (
    <View style={{overflow: 'hidden', ...containerStyle}}>
      <TouchableNativeFeedback
        onPress={disabled ? null : onPress}
        disabled={disabled}
        background={TouchableNativeFeedback.SelectableBackground()}
        style={{padding: 8}}>
        <View style={[style, disabled && {opacity: 0.5}]}>{children}</View>
      </TouchableNativeFeedback>
    </View>
  );
};

FilledTouchable.propTypes = {
  children: PropTypes.node,
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
  style: PropTypes.any,
  containerStyle: PropTypes.any,
};

export const OutlineTouchable = props => {
  const {children, onPress, disabled, style, rippleColor, containerStyle, isUseBackground = false} = props;
  return (
    <View style={{overflow: 'hidden', ...containerStyle}}>
      <TouchableNativeFeedback
        onPress={disabled ? null : onPress}
        disabled={disabled}
        background={TouchableNativeFeedback.Ripple(rippleColor, false)}>
        <View style={[style, disabled && isUseBackground ? {opacity: 0.5} : {opacity: 1}]}>{children}</View>
      </TouchableNativeFeedback>
    </View>
  );
};

OutlineTouchable.propTypes = {
  children: PropTypes.node,
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
  style: PropTypes.any,
  containerStyle: PropTypes.any,
  rippleColor: PropTypes.any,
  isUseBackground: PropTypes.bool,
};

export const Touchable = props => {
  const {children, onPress, disabled, style, hitSlop, className, index, opacity, activeOpacity, reff} = props;
  return (
    <TouchableOpacity
      ref={reff}
      key={index}
      className={className}
      style={style}
      activeOpacity={activeOpacity}
      opacity={opacity}
      onPress={disabled ? null : onPress}
      hitSlop={hitSlop ? hitSlop : {}}
      disabled={disabled}>
      {children}
    </TouchableOpacity>
  );
};

Touchable.defaultProps = {
  opacity: 0.5,
  activeOpacity: 0.8,
};

Touchable.propTypes = {
  children: PropTypes.node,
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
  style: PropTypes.any,
  hitSlop: PropTypes.any,
  className: PropTypes.any,
  index: PropTypes.any,
  opacity: PropTypes.any,
  activeOpacity: PropTypes.any,
  reff: PropTypes.any,
};
