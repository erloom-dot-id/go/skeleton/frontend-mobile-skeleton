import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {Colors, Typography} from '../../utils';
import PropTypes from 'prop-types';

// -------------------- TEXT BLACK-------------------- //
export const TextBlack = props => {
  const {className, text, numberOfLines, size, color, lineHeight, style, onTextLayout} = props;
  return (
    <Text
      className={className}
      ellipsizeMode="tail"
      onTextLayout={onTextLayout}
      style={[styles.blackText, {fontSize: size, color: color, lineHeight: lineHeight}, style]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

TextBlack.defaultProps = {
  size: Typography.FONT_SIZE_14,
  leftIconSize: 14,
  color: Colors.NETRAL900,
};

TextBlack.propTypes = {
  className: PropTypes.string,
  text: PropTypes.any.isRequired,
  numberOfLines: PropTypes.number,
  size: PropTypes.any,
  onTextLayout: PropTypes.func,
  color: PropTypes.any,
  lineHeight: PropTypes.number,
  style: PropTypes.any,
};

// -------------------- TEXT BOLD-------------------- //
export const TextBold = props => {
  const {className, text, numberOfLines, size, color, lineHeight, style, onTextLayout} = props;
  return (
    <Text
      className={className}
      onTextLayout={onTextLayout}
      style={[styles.boldText, {fontSize: size, color: color, lineHeight: lineHeight}, style]}
      ellipsizeMode="tail"
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

TextBold.defaultProps = {
  size: Typography.FONT_SIZE_14,
  leftIconSize: 14,
  color: Colors.NETRAL900,
};

TextBold.propTypes = {
  className: PropTypes.string,
  text: PropTypes.any,
  numberOfLines: PropTypes.number,
  size: PropTypes.any,
  onTextLayout: PropTypes.func,
  color: PropTypes.any,
  lineHeight: PropTypes.number,
  style: PropTypes.any,
};

// -------------------- TEXT SEMIBOLD-------------------- //
export const TextSemiBold = props => {
  const {className, text, numberOfLines, size, color, lineHeight, style, onTextLayout} = props;
  return (
    <Text
      className={className}
      ellipsizeMode="tail"
      onTextLayout={onTextLayout}
      style={[styles.semiBoldText, {fontSize: size, color: color, lineHeight: lineHeight}, style]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

TextSemiBold.defaultProps = {
  size: Typography.FONT_SIZE_14,
  leftIconSize: 14,
  color: Colors.NETRAL900,
};

TextSemiBold.propTypes = {
  className: PropTypes.string,
  text: PropTypes.any.isRequired,
  numberOfLines: PropTypes.number,
  size: PropTypes.any,
  onTextLayout: PropTypes.func,
  color: PropTypes.any,
  lineHeight: PropTypes.number,
  style: PropTypes.any,
};

// -------------------- TEXT REGULAR -------------------- //
export const TextRegular = props => {
  const {className, text = '-', size, color, lineHeight, style, onTextLayout, numberOfLines, onPress} = props;
  return (
    <Text
      onPress={onPress}
      className={className}
      ellipsizeMode="tail"
      onTextLayout={onTextLayout}
      numberOfLines={numberOfLines}
      style={[styles.regularText, {fontSize: size, color: color, lineHeight: lineHeight}, style]}>
      {text}
    </Text>
  );
};

TextRegular.defaultProps = {
  size: Typography.FONT_SIZE_14,
  leftIconSize: 14,
  color: Colors.NETRAL900,
};

TextRegular.propTypes = {
  className: PropTypes.string,
  text: PropTypes.any,
  numberOfLines: PropTypes.number,
  size: PropTypes.any,
  onTextLayout: PropTypes.func,
  color: PropTypes.any,
  lineHeight: PropTypes.number,
  style: PropTypes.any,
  onPress: PropTypes.func,
};

// -------------------- TEXT LIGHT -------------------- //
export const TextLight = props => {
  const {className, text, numberOfLines, size, color, lineHeight, style, onTextLayout} = props;
  return (
    <Text
      className={className}
      ellipsizeMode="tail"
      onTextLayout={onTextLayout}
      style={[
        styles.lightText,
        {
          fontSize: size,
          color: color,
          lineHeight: lineHeight,
        },
        style,
      ]}
      numberOfLines={numberOfLines}>
      {text}
    </Text>
  );
};

TextLight.defaultProps = {
  size: Typography.FONT_SIZE_14,
  leftIconSize: 14,
  color: Colors.NETRAL900,
};

TextLight.propTypes = {
  className: PropTypes.string,
  text: PropTypes.string.isRequired,
  size: PropTypes.any,
  color: PropTypes.any,
  onTextLayout: PropTypes.func,
  lineHeight: PropTypes.number,
  style: PropTypes.any,
  numberOfLines: PropTypes.number,
};

export const styles = StyleSheet.create({
  regularText: Typography.FONT_REGULAR,
  semiBoldText: Typography.FONT_SEMIBOLD,
  boldText: Typography.FONT_BOLD,
  lightText: Typography.FONT_LIGHT,
  blackText: Typography.FONT_BLACK,
});
