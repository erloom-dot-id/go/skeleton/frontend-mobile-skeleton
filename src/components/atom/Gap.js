import React from 'react';
import {View} from 'react-native';
import PropTypes from 'prop-types';

export const Gap = props => {
  const {className, width, height, style, backgroundColor, fullWidth} = props;
  return (
    <View
      style={{
        backgroundColor,
        marginHorizontal: fullWidth ? -20 : 0,
        zIndex: 100,
        ...style,
      }}>
      <View
        className={className}
        style={{
          backgroundColor,
          width: width,
          height: height,
        }}
      />
    </View>
  );
};

Gap.defaultProps = {
  backgroundColor: 'transparent',
  width: 0,
  height: 0,
  fullWidth: false,
};

Gap.propTypes = {
  className: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  style: PropTypes.any,
  backgroundColor: PropTypes.any,
  fullWidth: PropTypes.bool,
};
