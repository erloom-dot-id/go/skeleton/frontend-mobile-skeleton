import React, {Fragment} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import PropTypes from 'prop-types';
import {Colors} from '../../utils';
import FocusAwareStatusBar from './FocusAwareStatusBar';
import {StyleSheet} from 'react-native';

const Container = props => {
  const {style = null, backgroundColor, children, onLayout, className, statusBarColor, statusBarStyle} = props;
  return (
    <Fragment>
      <FocusAwareStatusBar backgroundColor={statusBarColor} barStyle={statusBarStyle} />
      <SafeAreaView className={`flex-1 ${className}`} style={[style, styles(backgroundColor)]} onLayout={onLayout}>
        {children}
      </SafeAreaView>
    </Fragment>
  );
};

const styles = backgroundColor =>
  StyleSheet.create({
    backgroundColor: backgroundColor,
  });

Container.defaultProps = {
  statusBarColor: 'transparent',
  statusBarStyle: 'dark-content',
  backgroundColor: Colors.NETRAL0,
};

Container.propTypes = {
  backgroundColor: PropTypes.string,
  style: PropTypes.any,
  children: PropTypes.node,
  onLayout: PropTypes.func,
  className: PropTypes.string,
  statusBarColor: PropTypes.any,
  statusBarStyle: PropTypes.string,
};

export default Container;
