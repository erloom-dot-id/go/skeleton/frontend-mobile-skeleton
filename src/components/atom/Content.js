import {KeyboardAvoidingView, ScrollView, View} from 'react-native';
import PropTypes from 'prop-types';
import {Colors} from '../../utils';

const Content = props => {
  const {
    className,
    children,
    style,
    backgroundColor,
    bounces,
    contentStyle,
    scrollEnabled,
    onScroll,
    reff,
    stickyHeaderIndices,
  } = props;
  return (
    <KeyboardAvoidingView behavior="height" style={{flex: 1}}>
      {scrollEnabled ? (
        <ScrollView
          ref={reff}
          stickyHeaderIndices={stickyHeaderIndices}
          scrollEventThrottle={16}
          keyboardDismissMode="interactive"
          alwaysBounceVertical={false}
          automaticallyAdjustContentInsets
          overScrollMode="never"
          onScroll={onScroll}
          disableIntervalMomentum
          bouncesZoom={false}
          contentContainerStyle={{padding: 16, flexGrow: 1, ...contentStyle}}
          className={className}
          style={{backgroundColor: backgroundColor, ...style}}
          bounces={bounces}
          showsVerticalScrollIndicator={false}>
          {children}
        </ScrollView>
      ) : (
        <View className={className} style={{backgroundColor: backgroundColor, ...style}}>
          {children}
        </View>
      )}
    </KeyboardAvoidingView>
  );
};

Content.defaultProps = {
  bounces: true,
  backgroundColor: Colors.NETRAL0,
  scrollEnabled: true,
};

Content.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  style: PropTypes.any,
  bounces: PropTypes.bool,
  contentStyle: PropTypes.any,
  backgroundColor: PropTypes.any,
  scrollEnabled: PropTypes.bool,
  onScroll: PropTypes.any,
  reff: PropTypes.any,
  stickyHeaderIndices: PropTypes.any,
};

export default Content;
