import {API_BASE_URL} from '@env';
import axios from 'axios';

const API = async (
  url,
  options = {
    method: 'GET' || 'DELETE',
    body: {},
    head: {},
    params: {},
    responseType: 'json',
  },
) => {
  const log = {
    uri: `${API_BASE_URL}${url}`,
    method: options?.method,
    body: options?.body ?? null,
    params: options?.params,
  };
  console.log('[LOG] AXIOS Config', log);
  const request = {
    baseURL: API_BASE_URL,
    method: options.method,
    timeout: 10000,
    url,
    headers: options.head,
    responseType: options.responseType ?? 'json',
    params: options.params ?? {},
  };
  if (request.method === 'POST' || request.method === 'PUT') {
    request.data = options.body;
  }
  return axios(request);
};

export default API;
