import {createWithEqualityFn} from 'zustand/traditional';
import {createJSONStorage, persist} from 'zustand/middleware';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const useAuth = createWithEqualityFn(
  persist(
    (set, get) => ({
      isLogin: false,
      setLogin: data => {
        set({isLogin: data});
      },
    }),
    {
      name: 'auth-storage', // unique name
      storage: createJSONStorage(() => AsyncStorage),
    },
  ),
);
