import React, {Fragment, useEffect, useRef} from 'react';
import {Image, Platform, SafeAreaView, StatusBar, View, Animated} from 'react-native';
import {Colors} from '../../utils';
import {logoWhite, logoTextWhite, logoRN} from '../../assets';

const Splash = ({isStart}) => {
  const logoAnime = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.parallel([
      Animated.spring(logoAnime, {
        toValue: 1,
        tension: 10,
        friction: 2,
        duration: 1000,
        useNativeDriver: false,
      }).start(() => {
        setTimeout(() => {
          isStart(false);
        }, 2500);
      }),
    ]);
  });

  return (
    <Fragment>
      <StatusBar hidden />
      <SafeAreaView style={[styles.page.wrapper, {paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0, flex: 1}]}>
        <View style={styles.page.logo}>
          <Animated.View
            style={{
              opacity: logoAnime,
              top: logoAnime.interpolate({
                inputRange: [0, 1],
                outputRange: [80, 0],
              }),
            }}>
            <Image source={logoRN} style={{width: 110, height: 90, marginBottom: 15}} resizeMethod={'scale'} />
          </Animated.View>
        </View>
      </SafeAreaView>
    </Fragment>
  );
};

const styles = {
  page: {
    wrapper: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#000000',
    },
    logo: {
      width: 200,
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
};

export default Splash;
