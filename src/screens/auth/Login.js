import { Fragment, useEffect } from 'react';
import {Container} from '../../components';
import LoginTemplate from '../../components/templates/auth/LoginTemplate';
import API from '../../services/axiosConfig';

const Login = ({navigation}) => {
  const testAPI = async () => {
    var headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
    const res = await API("/healthz", {
      method: "GET",
      head: headers
    });

    console.log(res.data);
  };

  const testError = () => {
    throw new Error("My first Sentry error!")
  }

  useEffect(() => {
    console.log("Login Page");
    testAPI();
    // testError();
  }, []);

  return (
    <Container>
      <LoginTemplate navigation={navigation}/>
    </Container>
  );
};

export default Login;
