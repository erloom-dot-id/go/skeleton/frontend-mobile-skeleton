import {Container} from '../../components';
import RegisterTemplate from '../../components/templates/auth/RegisterTemplate';

const Register = ({navigation}) => {
  return (
    <Container> 
      <RegisterTemplate navigation={navigation} />
    </Container>
  );
};

export default Register;
