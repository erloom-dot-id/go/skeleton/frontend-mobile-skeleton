// ------------------ BLUE ------------------
export const BLUE900 = '#1a237e';
export const BLUE800 = '#283593';
export const BLUE700 = '#303f9f';
export const BLUE600 = '#295ECC'; // current
export const BLUE500 = '#3f51b5';
export const BLUE400 = '#2E90FA'; // current
export const BLUE300 = '#7986cb';
export const BLUE200 = '#9fa8da';
export const BLUE100 = '#c5cae9';
export const BLUE50 = '#EFF8FF'; //current

// ----------------- NETRAL -----------------
export const NETRAL900 = '#1E1E1F';
export const NETRAL800 = '#37474f';
export const NETRAL700 = '#646566'; // current
export const NETRAL600 = '#546e7a';
export const NETRAL500 = '#607d8b';
export const NETRAL400 = '#8C8D8F'; // current
export const NETRAL300 = '#90a4ae';
export const NETRAL200 = '#b0bec5';
export const NETRAL100 = '#BEBFC2'; // current
export const NETRAL50 = '#E6E8EB'; // current
export const NETRAL40 = '#F5F6F7'; // current
export const NETRAL0 = '#FFFFFF';

// ------------------ RED ------------------
export const RED1000 = '#9D1101'; // current
export const RED900 = '#C2000D'; // current
export const RED800 = '#CF1701'; // current
export const RED700 = '#d32f2f';
export const RED600 = '#e53935';
export const RED500 = '#f44336';
export const RED400 = '#ef5350';
export const RED300 = '#e57373';
export const RED200 = '#ef9a9a';
export const RED100 = '#ffcdd2';
export const RED50 = '#FFEBEC';

// ----------------- YELLOW -----------------
export const YELLOW900 = '#f57f17';
export const YELLOW800 = '#f9a825';
export const YELLOW700 = '#fbc02d';
export const YELLOW600 = '#fdd835';
export const YELLOW500 = '#ffeb3b';
export const YELLOW400 = '#ffee58';
export const YELLOW300 = '#fff176';
export const YELLOW200 = '#fff59d';
export const YELLOW100 = '#fff9c4';
export const YELLOW50 = '#fffde7';

// ------------------ GREEN ------------------
export const GREEN900 = '#1b5e20';
export const GREEN800 = '#2e7d32';
export const GREEN700 = '#039855'; // current
export const GREEN600 = '#12B76A'; // current
export const GREEN500 = '#2AB95E'; // current
export const GREEN400 = '#66bb6a';
export const GREEN300 = '#81c784';
export const GREEN200 = '#A6F4C5'; // current
export const GREEN100 = '#D1FADF'; // current
export const GREEN50 = '#ECFDF3'; // current

// ----------------- PURPLE -----------------
export const PURPLE900 = '#311b92';
export const PURPLE800 = '#4527a0';
export const PURPLE700 = '#512da8';
export const PURPLE600 = '#5e35b1';
export const PURPLE500 = '#673ab7';
export const PURPLE400 = '#7A5AF8'; //current
export const PURPLE300 = '#9575cd';
export const PURPLE200 = '#9891D3';
export const PURPLE100 = '#d1c4e9';
export const PURPLE50 = '#F4F3FF'; //current

// ------------------ GREY ------------------
export const GREY900 = '#212121';
export const GREY800 = '#424242';
export const GREY700 = '#616161';
export const GREY600 = '#757575';
export const GREY500 = '#9e9e9e';
export const GREY400 = '#bdbdbd';
export const GREY300 = '#e0e0e0';
export const GREY200 = '#eeeeee';
export const GREY100 = '#f5f5f5';
export const GREY50 = '#fafafa';

// ------------------ TEAL ------------------
export const TEAL900 = '#004d40';
export const TEAL800 = '#00695c';
export const TEAL700 = '#00796b';
export const TEAL600 = '#00897b';
export const TEAL500 = '#009688';
export const TEAL400 = '#26a69a';
export const TEAL300 = '#4db6ac';
export const TEAL200 = '#80cbc4';
export const TEAL100 = '#b2dfdb';
export const TEAL50 = '#e0f2f1';

// ------------------ ORANGE ------------------
export const ORANGE900 = '#e65100';
export const ORANGE800 = '#FB6514'; // current
export const ORANGE700 = '#FF6A39'; // current
export const ORANGE600 = '#fb8c00';
export const ORANGE500 = '#EA9437'; // current
export const ORANGE400 = '#ffa726';
export const ORANGE300 = '#ffb74d';
export const ORANGE200 = '#ffcc80';
export const ORANGE100 = '#ffe0b2';
export const ORANGE50 = '#FFF6ED'; // current

// ------------------ PINK ------------------
export const PINK900 = '#880e4f';
export const PINK800 = '#ad1457';
export const PINK700 = '#c2185b';
export const PINK600 = '#d81b60';
export const PINK500 = '#F63D68'; //current
export const PINK400 = '#ec407a';
export const PINK300 = '#f06292';
export const PINK200 = '#f48fb1';
export const PINK100 = '#f8bbd0';
export const PINK50 = '#FFF1F3'; //current

// ------------------ TRANSPARENT ------------------
export const TRANSPARENT = 'rgba(0, 0, 0, 0.0)';
