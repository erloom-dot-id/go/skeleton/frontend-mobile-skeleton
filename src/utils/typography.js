import {PixelRatio} from 'react-native';

const scaleFont = size => size * PixelRatio.getFontScale();

// FONT FAMILY
export const FONT_FAMILY_BLACK = 'source-sans-pro.black';
export const FONT_FAMILY_BOLD = 'source-sans-pro.bold';
export const FONT_FAMILY_SEMIBOLD = 'source-sans-pro.semibold';
export const FONT_FAMILY_REGULAR = 'source-sans-pro.regular';
export const FONT_FAMILY_LIGHT = 'source-sans-pro.light';

// FONT SIZE
export const FONT_SIZE_36 = scaleFont(36);
export const FONT_SIZE_24 = scaleFont(24);
export const FONT_SIZE_20 = scaleFont(20);
export const FONT_SIZE_18 = scaleFont(18);
export const FONT_SIZE_16 = scaleFont(16);
export const FONT_SIZE_14 = scaleFont(14);
export const FONT_SIZE_12 = scaleFont(12);
export const FONT_SIZE_10 = scaleFont(10);
export const FONT_SIZE_11 = scaleFont(11);
export const FONT_SIZE_9 = scaleFont(9);
export const FONT_SIZE_8 = scaleFont(8);
export const FONT_SIZE_6 = scaleFont(6);

// LINE HEIGHT
export const LINE_HEIGHT_34 = scaleFont(34);
export const LINE_HEIGHT_28 = scaleFont(28);
export const LINE_HEIGHT_24 = scaleFont(24);
export const LINE_HEIGHT_22 = scaleFont(22);
export const LINE_HEIGHT_20 = scaleFont(20);
export const LINE_HEIGHT_18 = scaleFont(18);
export const LINE_HEIGHT_16 = scaleFont(16);
export const LINE_HEIGHT_14 = scaleFont(14);

// FONT STYLE
export const FONT_BLACK = {
  fontFamily: FONT_FAMILY_BLACK,
};

export const FONT_BOLD = {
  fontFamily: FONT_FAMILY_BOLD,
};

export const FONT_SEMIBOLD = {
  fontFamily: FONT_FAMILY_SEMIBOLD,
};

export const FONT_REGULAR = {
  fontFamily: FONT_FAMILY_REGULAR,
};

export const FONT_LIGHT = {
  fontFamily: FONT_FAMILY_LIGHT,
};
