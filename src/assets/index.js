//LOGO
export Logo from './logo/caready-logo.png';
export logoWhite from './logo/logo_white.png';
export logoTextWhite from './logo/logo_text_white.png';
export logoRN from './logo/reactnative_logo.png';

//ANIMATION
export const LiveAnimation = require('./animation/LiveAnimation.json');

//ICON
export HomeIcon from './icons/HomeIcon.svg';
export CalendarIcon from './icons/CalendarIcon.svg';
export TicketIcon from './icons/TicketIcon.svg';
export ArrowLeftRightIcon from './icons/ArrowLeftRightIcon.svg';
export UserIcon from './icons/UserIcon.svg';
export CopyIcon from './icons/CopyIcon.svg';
export EyeOpenIcon from './icons/EyeOpenIcon.svg';
export EyeCloseIcon from './icons/EyeCloseIcon.svg';
export SearchIcon from './icons/SearchIcon.svg';
export BellsIcon from './icons/BellsIcon.svg';
export ArrowLeftIcon from './icons/ArrowLeftIcon.svg';
export CarIcon from './icons/CarIcon.svg';
export MotorcycleIcon from './icons/MotorcycleIcon.svg';
export MonitorIcon from './icons/MonitorIcon.svg';
export HandCoinIcon from './icons/HandCoinIcon.svg';
export ChevronRightIcon from './icons/ChevronRightIcon.svg';
export ClockIcon from './icons/ClockIcon.svg';
export LocationIcon from './icons/LocationIcon.svg';
export GoogleIcon from './icons/GoogleIcon.svg';
export FacebookIcon from './icons/FacebookIcon.svg';
export FilterIcon from './icons/FilterIcon.svg';
export CloseIcon from './icons/CloseIcon.svg';
export More2LineIcon from './icons/More2LineIcon.svg';
export LoveIcon from './icons/LoveIcon.svg';
export EyeIcon from './icons/EyeIcon.svg';
export WarningFIlledIcon from './icons/WarningFIlledIcon.svg';
export SuccessFilledIcon from './icons/SuccessFilledIcon.svg';
export TagFilledIcon from './icons/TagFilledIcon.svg';
export AlaramIcon from './icons/AlaramIcon.svg';
export SettingIcon from './icons/SettingIcon.svg';
export CustomerIcon from './icons/CustomerIcon.svg';
export FaqIcon from './icons/FaqIcon.svg';
export InfoIcon from './icons/InfoIcon.svg';
export ListIcon from './icons/ListIcon.svg';
export ListMenuIcon from './icons/ListMenuIcon.svg';
export LockIcon from './icons/LockIcon.svg';
export MapIcon from './icons/MapIcon.svg';
export ProxyBidIcon from './icons/ProxyBidIcon.svg';
export DocumentIcon from './icons/DocumentIcon.svg';
export LogoutIcon from './icons/LogoutIcon.svg';
export PenIcon from './icons/PenIcon.svg';
export FinanceIcon from './icons/FinanceIcon.svg';
export LockPasswordIcon from './icons/LockPasswordIcon.svg';


//IMAGE
export const BGTag = require('./images/BGTag.png');
export const BGTag2 = require('./images/BGTag2.png');
export const BGSpecialNIPL = require('./images/BGSpecialNIPL.png');

//DUMMY
export const Banner1 = require('./dummy/Banner1.jpg');
export const Banner2 = require('./dummy/Banner2.jpg');
export const Banner3 = require('./dummy/Banner3.jpg');
export const Banner4 = require('./dummy/Banner4.jpg');
export const BannerProduct1 = require('./dummy/BannerProduct1.jpg');
export const BannerProduct2 = require('./dummy/BannerProduct2.jpg');
export const Car1 = require('./dummy/Car1.jpg');
export const Car2 = require('./dummy/Car2.jpg');
export const Car3 = require('./dummy/Car3.jpeg');
export const Car4 = require('./dummy/Car4.jpg');
export const Car5 = require('./dummy/Car5.jpg');
export const User = require('./dummy/UserFoto.png');
