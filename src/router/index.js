import {createStackNavigator} from '@react-navigation/stack';
import {useState} from 'react';
import {Splash} from '../screens/splash';
import MainNavigation from './MainNavigation';
import { Container, TextBlack } from '../components';

const RootStack = createStackNavigator();
const RootStackScreen = () => {
  return (
    // <Container>
    //     <TextBlack text={"testing"}/>
    // </Container>
    <RootStack.Navigator screenOptions={{headerShown: false}} initialRouteName={'Main'}>
      <RootStack.Screen name="Main" component={MainNavigation} options={{animationEnabled: false}} />
    </RootStack.Navigator>
  );
};

const Router = () => {
  const [isStart, setIsStart] = useState(true);

  if (isStart) {
    return <Splash isStart={setIsStart} />;
  }

  return <RootStackScreen />;
};

export default Router;
