import React from 'react';
import {Host} from 'react-native-portalize';
import {createStackNavigator} from '@react-navigation/stack';
import {Login, Register} from '../screens/auth';

const Stack = createStackNavigator();
const MainNavigation = () => {
  return (
    <Host>
      <Stack.Navigator initialRouteName="TabNav">
        <Stack.Screen name="Login" component={Login} options={{headerShown: false}} />
        <Stack.Screen name="Register" component={Register} options={{headerShown: false}} />
      </Stack.Navigator>
    </Host>
  );
};

export default MainNavigation;
