import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {SafeAreaProvider, initialWindowMetrics} from 'react-native-safe-area-context';
import Router from './router';
import {QueryClient, QueryClientProvider} from 'react-query';
import * as Sentry from "@sentry/react-native";
import {SENTRY_DSN, SENTRY_SAMPLE_RATE} from '@env';

Sentry.init({
  dsn: SENTRY_DSN,
  // Set tracesSampleRate to 1.0 to capture 100% of transactions for performance monitoring.
  // We recommend adjusting this value in production.
  tracesSampleRate: SENTRY_SAMPLE_RATE*1,
});

const App = () => {
  const queryClient = new QueryClient();
  queryClient.setDefaultOptions({
    queries: {
      refetchOnWindowFocus: false,
      refetchOnmount: false,
      onError: err => console.log(err),
    },
  });

  return (
    <NavigationContainer>
      <QueryClientProvider client={queryClient}>
        <SafeAreaProvider initialMetrics={initialWindowMetrics}>
          <Router />
        </SafeAreaProvider>
      </QueryClientProvider>
    </NavigationContainer>
  );
};

export default Sentry.wrap(App);